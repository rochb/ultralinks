package me.sharkz.ultralinks.loaders;

import org.bukkit.configuration.file.FileConfiguration;

public interface Loader<T> {

    /**
     * Load object from configuration
     *
     * @param config bukkit conf
     * @param path   path to object
     * @return object
     */
    T load(FileConfiguration config, String path);

    /**
     * Check if path is valid
     *
     * @param config config
     * @param path   to object
     * @return true if path is valid
     */
    boolean isValid(FileConfiguration config, String path);
}
