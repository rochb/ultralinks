package me.sharkz.ultralinks.loaders;

import me.sharkz.ultralinks.links.Link;
import org.bukkit.configuration.file.FileConfiguration;

public class LinkLoader implements Loader<Link> {

    @Override
    public Link load(FileConfiguration config, String path) {
        return new Link(config.getString(path + ".message"),
                config.getString(path + ".link"),
                config.getString(path + ".hover"),
                config.getString(path + ".command"),
                config.getString(path + ".permission"));
    }

    @Override
    public boolean isValid(FileConfiguration config, String path) {
        return config.getString(path + ".message") != null
                && config.getString(path + ".link") != null
                && config.getString(path + ".command") != null;
    }

}
