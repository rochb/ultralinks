package me.sharkz.ultralinks.commands;

import me.sharkz.ultralinks.UL;
import me.sharkz.ultralinks.utils.Util;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainCmd extends UCmd {

    public MainCmd() {
        super("ultralinks", "Secret command for server admins :)", "/ul", Arrays.asList("ul", "links"));
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if(args.length == 1 && (args[0].equalsIgnoreCase("rl") || args[0].equalsIgnoreCase("reload"))){
            if(!sender.hasPermission("ultralinks.reload")){
                Util.sendMessage(sender, "noPermission", "%prefix% &eYou don't have the permission.");
                return true;
            }
            UL.I.reload();
            Util.sendMessage(sender, "reloaded", "%prefix% &a&lConfiguration has been reloaded !");
        }else
            sender.sendMessage(Util.color(UL.getPrefix() + "&r&7 Version &a&l" + UL.I.getDescription().getVersion()));
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        List<String> tmp = new ArrayList<>();
        if(args.length == 0 && sender.hasPermission("ultralinks.reload"))
            tmp.add("reload");
        return tmp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return false;
    }

    @Override
    public Plugin getPlugin() {
        return UL.I;
    }
}
