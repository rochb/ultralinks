package me.sharkz.ultralinks.commands;


import me.sharkz.ultralinks.UL;
import me.sharkz.ultralinks.utils.Util;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginIdentifiableCommand;

import java.util.List;

public abstract class UCmd extends Command implements CommandExecutor, PluginIdentifiableCommand {

    private boolean register = false;

    public UCmd(String name, String description, String usageMessage, List<String> aliases) {
        super(name, description, usageMessage, aliases);
        registerCommand();
    }

    /**
     * Register the command
     */
    public void registerCommand() {
        CommandMap commandMap = null;
        try {
            commandMap = (CommandMap) Util.getPrivateField(UL.I.getServer().getPluginManager(), "commandMap");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            UL.L.warning("Cannot register command : " + getName() + " !");
            e.printStackTrace();
        }
        if (!register && commandMap != null)
            register = commandMap.register(UL.I.getName(), this);
    }
}
