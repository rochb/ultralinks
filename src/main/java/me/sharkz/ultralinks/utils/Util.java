package me.sharkz.ultralinks.utils;

import me.sharkz.ultralinks.UL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.configuration.file.FileConfiguration;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Util {

    private static final FileConfiguration config = UL.I.getConfig();

    /**
     * Color message
     *
     * @param message to color
     * @return colored message
     */
    public static String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Color list of message
     *
     * @param messages list to color
     * @return colored list of message
     */
    public static List<String> color(List<String> messages) {
        return messages.stream().map(Util::color).collect(Collectors.toList());
    }

    /**
     * Reverse color
     *
     * @param message to reverse
     * @return reversed message
     */
    public static String reverseColor(String message) {
        return ChatColor.stripColor(message);
    }

    /**
     * Reverse color
     *
     * @param messages to reverse
     * @return reversed messages
     */
    public static List<String> reverseColor(List<String> messages) {
        return messages.stream().map(Util::reverseColor).collect(Collectors.toList());
    }

    /**
     * Send message from configuration
     *
     * @param sender
     * @param id
     * @param def
     */
    public static void sendMessage(CommandSender sender, String id, String def) {
        if (config.isList(id) && config.getStringList(id).size() > 0)
            config.getStringList(id)
                    .stream()
                    .map(s -> s.replaceAll("%prefix%", UL.getPrefix()))
                    .map(Util::color)
                    .forEach(sender::sendMessage);
        else
            sender.sendMessage(color(config.getString(id, def).replaceAll("%prefix%", UL.getPrefix())));
    }

    /**
     * @param object
     * @param field
     * @return
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Object getPrivateField(Object object, String field) throws SecurityException,
            NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field objectField = clazz.getDeclaredField(field);
        objectField.setAccessible(true);
        Object result = objectField.get(object);
        objectField.setAccessible(false);
        return result;
    }
}
