package me.sharkz.ultralinks.links;

public class Link {

    private final String message;
    private final String link;
    private String hover;
    private final String command;
    private String permission;


    public Link(String message, String link, String hover, String command, String permission) {
        this.message = message;
        this.link = link;
        this.hover = hover;
        this.command = command.replace("/", "");
        this.permission = permission;
    }

    /**
     * Returns message related to link
     *
     * @return link message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Link of link x)
     *
     * @return link
     */
    public String getLink() {
        return link;
    }

    /**
     * Returns true if link has an hover message
     *
     * @return .
     */
    public boolean hasHover() {
        return hover != null;
    }

    /**
     * Returns hover message for link
     *
     * @return hover message
     */
    public String getHover() {
        return hover;
    }

    /**
     * Returns link command
     *
     * @return link command
     */
    public String getCommand() {
        return command;
    }

    /**
     * Returns true if link has a permission
     *
     * @return .
     */
    public boolean hasPermission() {
        return permission != null;
    }

    /**
     * Returns link permission
     *
     * @return permission
     */
    public String getPermission() {
        return permission;
    }
}
