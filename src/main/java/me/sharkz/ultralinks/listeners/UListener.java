package me.sharkz.ultralinks.listeners;

import me.sharkz.ultralinks.UL;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class UListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        if (player.hasPermission("ultralinks.reload")
                && UL.isNewVersion()
                && UL.isUpdateEnabled()) {
            TextComponent message = new TextComponent("A new version of ");
            message.setColor(ChatColor.GREEN);
            TextComponent pl1 = new TextComponent("ULTRA ");
            TextComponent pl2 = new TextComponent("LINKS ");
            pl1.setColor(ChatColor.RED);
            pl1.setBold(true);
            pl2.setColor(ChatColor.GREEN);
            pl2.setBold(true);
            message.addExtra(pl1);
            message.addExtra(pl2);
            TextComponent msg = new TextComponent("is available. ");
            msg.setColor(ChatColor.GREEN);
            message.addExtra(msg);
            TextComponent link = new TextComponent("Click here to download !");
            link.setColor(ChatColor.GOLD);
            link.setBold(true);
            link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/ultrawelcome-1-8-1-16-fully-customizable.82991/"));
            message.addExtra(link);
            player.spigot().sendMessage(message);
        }
    }
}
