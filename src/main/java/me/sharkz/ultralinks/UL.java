package me.sharkz.ultralinks;

import me.sharkz.ultralinks.commands.MainCmd;
import me.sharkz.ultralinks.links.LinksManager;
import me.sharkz.ultralinks.utils.UpdateChecker;
import me.sharkz.ultralinks.utils.Util;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public final class UL extends JavaPlugin {

    // Utils
    public static UL I;
    public static final Logger L = Bukkit.getLogger();

    // Configuration
    private static String prefix;
    private final int configVersion = 1;

    // Plugin update
    private static boolean newVersion = false;
    private static boolean updateEnabled = true;

    // Links
    private LinksManager linksManager;

    public UL() {
        I = this;
    }

    @Override
    public void onEnable() {
        // Header (start)
        L.info("========== ULTRA LINKS ==========");
        L.info("Developed by : Sharkz");
        L.info("Version : " + getDescription().getVersion());
        L.info("Do not hesitate to positively rate the plugin on spigotmc.org");

        // Update
        checkVersion();

        // Metrics
        new Metrics(this, 8800);

        // Configuration
        loadConfig();

        // Commands
        new MainCmd().registerCommand();

        // Links
        linksManager = new LinksManager(getConfig());
        linksManager.load();

        // Header (end)
        L.info("=================================");
    }

    @Override
    public void onDisable() {
        linksManager.unload();
        L.info("Thanks for using Ultra Links !");
    }

    /**
     * Reload plugin
     */
    public void reload() {
        linksManager.unload();
        reloadConfig();
        linksManager.load();
    }

    /**
     * Check if a new version of the plugin exist
     */
    private void checkVersion() {
        if (!updateEnabled) return;
        new UpdateChecker(this, 83727)
                .getVersion(s -> {
                    if (getDescription().getVersion().equalsIgnoreCase(s))
                        L.info("You're running the last version of UltraLinks !");
                    else {
                        L.warning("A new version of UltraLinks is available on spigotmc.org !");
                        newVersion = true;
                    }
                });
    }

    /**
     * Load configuration file
     */
    private void loadConfig() {
        if (!getDataFolder().exists()) getDataFolder().mkdir();
        saveDefaultConfig();
        if (getConfig().getInt("config_version") != configVersion) {
            L.warning("The configuration file is outdated !");
            try {
                getConfig().save(new File(getDataFolder(), "config.old.yml"));
                L.info("Your configuration has been saved & replaced !");
            } catch (IOException e) {
                L.warning("Cannot backup your configuration ...");
                e.printStackTrace();
            }
            new File(getDataFolder(), "config.yml").delete();
            saveDefaultConfig();
        }
        prefix = getConfig().getString("prefix", "&c&lULTRA&a&lLINKS &7|");
        updateEnabled = getConfig().getBoolean("checkUpdate", true);
    }

    /**
     * Returns plugin prefix
     *
     * @return prefix
     */
    public static String getPrefix() {
        return Util.color(prefix);
    }

    /**
     * Returns current config version
     *
     * @return config version
     */
    public int getConfigVersion() {
        return configVersion;
    }

    /**
     * Returns true if a new version is available
     *
     * @return
     */
    public static boolean isNewVersion() {
        return newVersion;
    }

    /**
     * Returns links manager
     *
     * @return links manager
     */
    public LinksManager getLinksManager() {
        return linksManager;
    }


    /**
     * Returns true if update check is enabled
     *
     * @return
     */
    public static boolean isUpdateEnabled() {
        return updateEnabled;
    }
}
